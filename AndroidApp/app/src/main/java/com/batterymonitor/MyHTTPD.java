package com.batterymonitor;

import android.app.DownloadManager;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import fi.iki.elonen.NanoHTTPD;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;


/**
 * borrowed from jens on 25.03.17.
 */
public class MyHTTPD extends NanoHTTPD {
    public static final int PORT = 8765;
    private int batteryLevel;
    private int charging;
    private int temp;
    private String chargingString;
    private DownloadManager.Request ProcessClockface;

    public MyHTTPD() throws IOException {
        super(PORT);
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public void setCharging(int charging) {
        this.charging = charging;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public void batteryLogic(){
        int newtemp;

        newtemp = (int) ((this.temp/1.8) + 32 )/ 10;
        this.temp = newtemp;
        if (this.charging == 0) {
            chargingString = "Not Charging.";
        }else if (this.charging == 1) {
            chargingString = "Fast Charging.";
        }else{
            chargingString = "Slow Charging.";
        }



    }

    @Override
    public Response serve(IHTTPSession session) {
        Log.i("TAG", "serve: "+session.getUri());
        String uri = session.getUri();
        batteryLogic();

        if (uri.equals("/text.text")) {
            String response = "" + batteryLevel + "%";
                    /**"<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<body>\n"
                    + "\n"
                    + "<p>"
                    + "Welcome to your phone's Battery info. page. <p>"
                    + "Here is some information about your phone. <p>"
                    + "It is currently ruuning with %"
                    + "<b>"
                    + batteryLevel
                    + "</b>"
                    + " Battery level. <p>"
                    + "The Phone is "
                    + "<b>"
                    + chargingString
                    + "</b>"
                    + "<p>"
                    + "The the battery is running at "
                    + "<b>"
                    + temp
                    + "</b>"
                    + " degrees F."
                    + "</body></html>";*/



           return newFixedLengthResponse(response);

        }

        return  null;
    }
}