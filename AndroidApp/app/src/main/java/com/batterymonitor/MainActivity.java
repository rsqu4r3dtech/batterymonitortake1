package com.batterymonitor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.widget.TextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static MyHTTPD server;
    private TextView txtIpAddress;
    private Button btnStart;
    private Button btnStop;
    private String ip;
    private String host;
    private TextView batteryInfo;


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button btnStart = findViewById(R.id.btnStart);
        final Button btnStop = findViewById(R.id.btnStop);
        final TextView txtIpAddress = findViewById(R.id.txtIpAddress);
        final String host = getString(R.string.host);
        batteryInfo = (TextView) findViewById(R.id.textViewBatteryInfo);
        this.registerReceiver(this.batteryinfoReceiver,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        try {
            server = new MyHTTPD();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                try {
                    server.start();
                    initIPAddress();
                    txtIpAddress.setText( host + ip + ":" + MyHTTPD.PORT);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                server.stop();
                txtIpAddress.setText("");
            }
        });
    }

   private BroadcastReceiver batteryinfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
            int icon_small =intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL,0);
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            int plugged =intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
            boolean present = intent.getExtras().getBoolean(BatteryManager.EXTRA_PRESENT);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS,0);
            String technology = intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
            int temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
            int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);

            batteryInfo.setText(
                    "Health: " + health + "\n" + "Icon Small :"
                            + icon_small + "\n" + "Level :" + level + "\n"
                            + "Plugged In :" + plugged + "\n" + "Present :" +present
                            + "\n" + "Scale :" +scale+ "\n" + "Status :" + status
                            + "\n" + "Technology :" + technology + "\n"
                            + "Temperature :" + temp + "\n" + "Voltage :" + voltage
                            + "\n");
           server.setBatteryLevel(level);
           server.setCharging(plugged);
           server.setTemp(temp);


        }
    };
    private void initIPAddress() throws IOException {

        ip = "http://localhost/";
        //
        Log.i("TAG", "onCreate: " + ip);
    }

}
