/*
 * Entry point for the watch app
 */
import document from "document";
import { peerSocket } from "messaging";

const fitbitText = document.getElementById("fitbitText");

const phoneText = document.getElementById("phoneText");

fitbitText.text = "Phone Battery is at..."
phoneText.text = "No New Messages.";
peerSocket.onmessage = evt => {
phoneText.text = evt.data;
  console.log(`App received: ${(evt.data)}`);}
