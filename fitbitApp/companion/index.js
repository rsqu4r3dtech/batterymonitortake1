/*
 * Entry point for the companion app
 */
import { peerSocket } from "messaging";
console.log("Companion code started");

var  stringToSend = "hi";

//Make sure we do not overfill the message buffer.
console.log("Max message size=" + peerSocket.MAX_MESSAGE_SIZE);

// Listen for the onopen event

peerSocket.onopen = function() {
  // Now we are ready to send or receive messages
}

//Create a method to retrieve and send the message to the watch when the Peer Socket is open.
function sendText(input){
if (peerSocket.readyState === peerSocket.OPEN) {
   peerSocket.send(input);
  console.log ("Message Sent");
}}
 
//Create a method to connect to the local server, get the output, and pass that to our companion app.
function getLevel(){
fetch('http://127.0.0.1:8765/text.text') //this is the server location.  I could not use "localhost" for some reason.
  //If the server gives an error or we cannot find the server, pass an error.
  .then(response => {
    if (!response.ok) {
      throw new Error("Problems..."); 
    }
    //If the server does not give an error, pass the text.
    return response.text();
  })
  //Create a promise to pass the text into the send Message method.
  .then(myText => {
  sendText(myText);
    console.log(myText);
  }) 
  //Prehaps this is redundant, but if we make it this far, and it still fails, we want to know.
  .catch(error => {
    console.error('There has been a problem with your fetch operation:', error);
  });
}
// Message socket closes
peerSocket.onclose = () => {
  console.log("Companion Socket Closed");
};


//Run the get message from phone method.
 getLevel()
